package com.fire.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lumingfu
 */
@SpringBootApplication
public class MqSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqSpringBootStarterApplication.class, args);
    }

}
