package com.fire.mq.rabbitmq;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @author Mr.Fire -lmf
 * @date 2021/8/15 17:01
 * @desc
 */
public class FireRabbitTemplate extends RabbitTemplate {

    private String name="fireMQ";


    public FireRabbitTemplate(ConnectionFactory connectionFactory,String name) {
        super(connectionFactory);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
