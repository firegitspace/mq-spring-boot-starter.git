package com.fire.mq.rabbitmq;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Mr.Fire -lmf
 * @date 2021/8/15 16:17
 * @desc
 */
@ConfigurationProperties(prefix = "fire.mq")
public class RabbitMqProperties {

    private String address = "localhost";

    private int port = 5672;

    private String userName;

    private String password;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
